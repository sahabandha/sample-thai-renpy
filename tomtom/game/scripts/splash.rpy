label splashscreen:
    scene black
    with Pause(0)
    if persistent.setted_language == False:
        menu:
            "UI Language you want to use?"

            "English":
                $renpy.change_language(None)
            "ไทย":
                $renpy.change_language("thai")
        
        $ persistent.setted_language = True

    show text "หนึ่งในสมาชิกเรารักเซย์กะ นำเสนอ..." with dissolve
    with Pause(1)
    hide text with dissolve

    with Pause(0)

    return
