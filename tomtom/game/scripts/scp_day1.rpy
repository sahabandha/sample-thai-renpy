define steps = 0

label scp_day1_m:

    scene black with dissolve

    yatie "scp day1 test!"

    "คุณเห็นบันไดชุดยาวทอดยาวไปข้างหน้าคุณ"

    "คุณเริ่มปีนขึ้นไป"

    hide black with dissolve

    label climbStairs:

        if steps >= 4:
            jump stairsTop

        else:

            "บันไดยังมีต่อ"

            $ steps += 1

            menu:

                "ปีนต่อไป":
                    jump climbStairs

    label stairsTop:

        "คุณไปถึงชั้นบนสุดของบันไดแล้ว!"

    # This ends the game.

    return
