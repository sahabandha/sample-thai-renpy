# The script of the game goes in this file.
define persistent.setted_language = False

call splashscreen from splash

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define yatie = Character('ยาจิเอะ')

# The game starts here.

label start:

    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    scene bg room

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "吉弔八千慧.png" to the images
    # directory.

    show 吉弔八千慧

    # These display lines of dialogue.

    yatie "คุณได้สร้างเกม Ren'Py ใหม่แล้ว."

    yatie "เมื่อเพิ่มเรื่องราว, รูปภาพ, และเพลงแล้ว, ก็เผยแพร่สู่โลกได้!"

    yatie "ไกจิกเด็กตายบนปากโอ่ง!"

    yatie "ก์กิ์ป์ปิ์ฎุญุน้าน้ำป้าปำป้ำปํ้ญญูฎูนู๋เป่าปี่"

    yatie "นายสังฆภัณฑ์ เฮงพิทักษ์ฝั่ง ผู้เฒ่าซึ่งมีอาชีพเป็นฅนขายฃวด ถูกตำรวจปฏิบัติการจับฟ้องศาล ฐานลักนาฬิกาคุณหญิงฉัตรชฎา ฌานสมาธิ"

    hide 吉弔八千慧

    call scp_day1_m from scp_day1

    # This ends the game.

    return
