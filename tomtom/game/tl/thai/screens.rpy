# TODO: Translation updated at 2021-11-27 17:11

translate thai strings:

    # game/screens.rpy:256
    old "Back"
    new "ย้อนกลับ"

    # game/screens.rpy:257
    old "History"
    new "ประวัติ"

    # game/screens.rpy:258
    old "Skip"
    new "ข้าม"

    # game/screens.rpy:259
    old "Auto"
    new "ออโต้"

    # game/screens.rpy:260
    old "Save"
    new "บันทึก"

    # game/screens.rpy:261
    old "Q.Save"
    new "Q.บันทึก"

    # game/screens.rpy:262
    old "Q.Load"
    new "Q.โหลด"

    # game/screens.rpy:263
    old "Prefs"
    new "ตั้งค่า"

    # game/screens.rpy:304
    old "Start"
    new "เริ่ม"

    # game/screens.rpy:312
    old "Load"
    new "โหลด"

    # game/screens.rpy:314
    old "Preferences"
    new "ตั้งค่า"

    # game/screens.rpy:318
    old "End Replay"
    new "จบการเล่นซ้ำ"

    # game/screens.rpy:322
    old "Main Menu"
    new "เมนูหลัก"

    # game/screens.rpy:324
    old "About"
    new "เกี่ยวกับ"

    # game/screens.rpy:329
    old "Help"
    new "ช่วยเหลือ"

    # game/screens.rpy:335
    old "Quit"
    new "ออก"

    # game/screens.rpy:476
    old "Return"
    new "ย้อนกลับ"

    # game/screens.rpy:560
    old "Version [config.version!t]\n"
    new "รุ่น [config.version!t]\n"

    # game/screens.rpy:566
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "สร้างโดย {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:602
    old "Page {}"
    new "หน้า {}"

    # game/screens.rpy:602
    old "Automatic saves"
    new "บันทึกอัตโนมัติ"

    # game/screens.rpy:602
    old "Quick saves"
    new "บันทึกด่วน"

    # game/screens.rpy:644
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %Y %B %d, %H:%M"

    # game/screens.rpy:644
    old "empty slot"
    new "สล็อตว่าง"

    # game/screens.rpy:661
    old "<"
    new "<"

    # game/screens.rpy:664
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:667
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:673
    old ">"
    new ">"

    # game/screens.rpy:730
    old "Display"
    new "จอ"

    # game/screens.rpy:731
    old "Window"
    new "หน้าต่าง"

    # game/screens.rpy:732
    old "Fullscreen"
    new "เต็มจอ"

    # game/screens.rpy:736
    old "Rollback Side"
    new "Rollback Side"

    # game/screens.rpy:737
    old "Disable"
    new "ไม่ใช้"

    # game/screens.rpy:738
    old "Left"
    new "ซ้าย"

    # game/screens.rpy:739
    old "Right"
    new "ขวา"

    # game/screens.rpy:744
    old "Unseen Text"
    new "ข้อความที่ยังไม่ได้เห็น"

    # game/screens.rpy:745
    old "After Choices"
    new "After Choices"

    # game/screens.rpy:746
    old "Transitions"
    new "Transitions"

    # game/screens.rpy:755
    old "UI Language"
    new "ภาษาของ UI"

    # game/screens.rpy:767
    old "Text Speed"
    new "ความเร็วข้อความ"

    # game/screens.rpy:771
    old "Auto-Forward Time"
    new "Auto-Forward Time"

    # game/screens.rpy:778
    old "Music Volume"
    new "ความดังเพลง"

    # game/screens.rpy:785
    old "Sound Volume"
    new "ความดังเสียง"

    # game/screens.rpy:791
    old "Test"
    new "ทดสอบ"

    # game/screens.rpy:795
    old "Voice Volume"
    new "ความดังเสียงพูด"

    # game/screens.rpy:806
    old "Mute All"
    new "ปิดเสียงทั้งหมด"

    # game/screens.rpy:925
    old "The dialogue history is empty."
    new "ประวัติการสนทนาว่างเปล่า"

    # game/screens.rpy:995
    old "Keyboard"
    new "แป้นพิมพ์"

    # game/screens.rpy:996
    old "Mouse"
    new "เมาส์"

    # game/screens.rpy:999
    old "Gamepad"
    new "เกมแพด"

    # game/screens.rpy:1012
    old "Enter"
    new "Enter"

    # game/screens.rpy:1013
    old "Advances dialogue and activates the interface."
    new "เลื่อนการสนทนา และใช้งานส่วนติดต่อ"

    # game/screens.rpy:1016
    old "Space"
    new "Space"

    # game/screens.rpy:1017
    old "Advances dialogue without selecting choices."
    new "เลื่อนบทสนทนา โดยไม่ต้องเลือกตัวเลือก"

    # game/screens.rpy:1020
    old "Arrow Keys"
    new "ปุ่มลูกษร"

    # game/screens.rpy:1021
    old "Navigate the interface."
    new "นำทางส่วนติดต่อผู้ใช้"

    # game/screens.rpy:1024
    old "Escape"
    new "Escape"

    # game/screens.rpy:1025
    old "Accesses the game menu."
    new "เข้าถึงเมนูเกม"

    # game/screens.rpy:1028
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1029
    old "Skips dialogue while held down."
    new "ข้ามบทสนทนาเมื่อกดข้าง"

    # game/screens.rpy:1032
    old "Tab"
    new "Tab"

    # game/screens.rpy:1033
    old "Toggles dialogue skipping."
    new "Toggle การข้ามบทสนทนา"

    # game/screens.rpy:1036
    old "Page Up"
    new "Page up"

    # game/screens.rpy:1037
    old "Rolls back to earlier dialogue."
    new "กรอไปยังบทสนทนาที่เก่ากว่า"

    # game/screens.rpy:1040
    old "Page Down"
    new "Page Down"

    # game/screens.rpy:1041
    old "Rolls forward to later dialogue."
    new "กรอไปยังบทสนทนาที่ใหม่กว่า"

    # game/screens.rpy:1045
    old "Hides the user interface."
    new "ซ่อนส่วนส่วนติดต่อผู้ใช้"

    # game/screens.rpy:1049
    old "Takes a screenshot."
    new "จับภาพ"

    # game/screens.rpy:1053
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1057
    old "Opens the accessibility menu."
    new "เปิดเมนูการช่วยเหลือการเข้าถึง"

    # game/screens.rpy:1063
    old "Left Click"
    new "คลิกซ้าย"

    # game/screens.rpy:1067
    old "Middle Click"
    new "คลิกกลาง"

    # game/screens.rpy:1071
    old "Right Click"
    new "คลิกขวา"

    # game/screens.rpy:1075
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # game/screens.rpy:1079
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # game/screens.rpy:1086
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1090
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # game/screens.rpy:1094
    old "Right Shoulder"
    new "Right Shoulder"

    # game/screens.rpy:1099
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1103
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1107
    old "Y/Top Button"
    new "Y/Top Button"

    # game/screens.rpy:1110
    old "Calibrate"
    new "Calibrate"

    # game/screens.rpy:1175
    old "Yes"
    new "ใช่"

    # game/screens.rpy:1176
    old "No"
    new "ไม่"

    # game/screens.rpy:1222
    old "Skipping"
    new "ข้าม"

    # game/screens.rpy:1445
    old "Menu"
    new "เมนู"
